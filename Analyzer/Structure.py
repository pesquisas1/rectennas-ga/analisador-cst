from decimal import Decimal

class Structure:

    def __init__(self, directory, name): #O diretório deve ser o mesmo da pasta
        self.frequencies = []
        self.s11 = []
        self.bands = []
        self.name = name
        self.directory = directory

        file = open(directory)
        lines = file.readlines()
        file.close()

        for line in lines:
            line = line.split()
            if(line == []): continue #Skip blank lines
            self.frequencies.append(float(line[0]))
            self.s11.append(float(line[1]))
        
        self.AnalyzeBands()
    
    def AnalyzeBands(self):
        temp = {}
        lowerS11 = 0
        lowerS11Frequency = 0
        totalFrequencies = len(self.frequencies)
        for i in range(totalFrequencies):
            if(self.s11[i] < lowerS11):
                lowerS11 = self.s11[i]
                lowerS11Frequency = self.frequencies[i]

            if(self.s11[i] <= -10): #Below -10 dB is a tunning frequency
                if("start" in temp.keys()):
                    if(i == totalFrequencies - 1): #A band can remain openned because simulation's end frequency
                        temp["end"] = self.frequencies[i]
                        temp["lowerS11Frequency"] = lowerS11Frequency
                        temp["lowerS11"] = lowerS11
                        self.bands.append(temp)

                        temp = {}
                        lowerS11 = 0
                else:
                    temp["start"] = self.frequencies[i]

            elif(self.s11[i] > -10):
                if("start" in temp.keys()):
                    temp["end"] = self.frequencies[i]
                    temp["lowerS11Frequency"] = lowerS11Frequency
                    temp["lowerS11"] = lowerS11
                    self.bands.append(temp)

                    temp = {}
                    lowerS11 = 0

    def GetIndex(self, frequency):
        startFrequency = self.frequencies[0]
        stepsize = self.frequencies[1] - self.frequencies[0]

        return int(round((frequency - startFrequency)/stepsize))

    def GetS11(self, frequencia):
        indice = self.GetIndex(frequencia)
        return self.s11[indice]


