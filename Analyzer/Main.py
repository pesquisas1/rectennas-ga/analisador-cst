import Structure, Results.Text as text, Results.Image as image

#Choose structures to be read

files = ["Circular Array", "Circular Array with SRR", "Circular Array with ELC", "Rectangular Array", "Rectangular Array with SRR", 
            "Rectangular Array with ELC", "SRR", "ELC", "Rectifier"]

names = ["Circular", "with SRR", "with ELC", "Rectangular", "with SRR", "with ELC", "SRR", "ELC", "Rectifier"]

#===========================================================================================

#Reading Structure

structures = []
for i in range(len(names)):
    structures.append(Structure.Structure("./Structures/%s.txt" %(files[i]), names[i]))
#=============================================================================================

#Instantiate Result Classes
text_result = text.Text(structures)
image_result = image.Image(structures)
image_result.configImage((20, 12), 128, 30, 5)

#=============================================================================================

#Relatório Textual
text_result.WriteBands()
text_result.WriteS11(28) #S11 at 28  GHz

#=============================================================================================

#Relatório Imagem
#image_result.GenerateReturnLossGraphic() #Graphic with all structures and all ranges
#image_result.GenerateReturnLossGraphic(frequencyRange=(20,30), marks=[25,28]) #Mark 25 GHz and 28 GHz

image_result.GenerateReturnLossGraphic(mask=[1,1,1,0,0,0,0,0,0]) #0 is hidden, 1 is visible. The order is the same of the array 'names'
image_result.GenerateReturnLossGraphic(mask=[0,0,0,1,1,1,0,0,0])
image_result.GenerateReturnLossGraphic(mask=[0,0,0,0,0,0,1,1,0], frequencyRange=(20,30)) #Meta were simulated with different ranges to see whole tunning bands
image_result.GenerateReturnLossGraphic(mask=[0,0,0,0,0,0,0,0,1])

#============================================================================================