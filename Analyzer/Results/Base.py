from datetime import datetime
import os

class Base:

    def __init__(self, structures):
        self.structures = structures
        self.now = str(datetime.now()).replace(":", "-")
        self.quantityStructures = len(structures)

        if(not(os.path.exists("Exits"))):
            os.mkdir("Exits")