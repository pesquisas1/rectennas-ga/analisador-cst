import sys, Results.Base as Base

class Text(Base.Base):

    def __init__(self, structures):
        super().__init__(structures)
        
        self.file = open("./Exits/Report - %s.txt" %(self.now), "w", encoding="utf8")
        self.file.write("Report generated in %s\n" %(self.now))
        self.file.write("Structures: ")

        for i in range(self.quantityStructures - 1):
            self.file.write("%s, " %(structures[i].name))
        
        self.file.write("%s\n\n\n" %(structures[self.quantityStructures - 1].name)) #Last one

    def __del__(self):
        self.file.close()

    def WriteBands(self):
        self.file.write(".:TUNNING BANDS\n")
        for structure in self.structures:
            self.file.write("-> %s\n" %(structure.name))
            self.file.write("Band Start | Band End | Lower S11 Frequency | Lower S11\n")
            for band in structure.bands:
                self.file.write("%f       | %f    | %f                | %f\n" %(band["start"], band["end"], 
                    band["lowerS11Frequency"], band["lowerS11"]))
            self.file.write("\n")
            
        self.file.write("\n")

    def WriteS11(self, frequency):
        self.file.write(".:S11 at %f GHz\n" %(frequency))
        for structure in self.structures:
            self.file.write("-> %s: %f\n" %(structure.name, structure.GetS11(frequency)))
        
        self.file.write("\n\n")
