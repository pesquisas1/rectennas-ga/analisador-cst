import sys, os, matplotlib.pyplot as plt, numpy as np, Results.Base as Base

class Image(Base.Base):

    def __init__(self, structures):
        super().__init__(structures)

        self.dir = "./Exits/" + self.now
        os.mkdir(self.dir)

        self.figsize = None
        self.dpi = None
        self.lineWidth = None

    def __del__(self):
        plt.close()

    def configImage(self, figSize, dpi, fontSize = 22, lineWidth=1, legendPos = "best"):
        self.figsize = figSize
        self.dpi = dpi
        self.lineWidth = lineWidth
        plt.rcParams['font.size'] = fontSize
        plt.rcParams["legend.loc"] = legendPos

    def GenerateReturnLossGraphic(self, mask = None, frequencyRange = None, marks =[]):
        plt.figure(figsize=self.figsize, dpi=self.dpi)
        legends = []

        if(mask == None): mask = np.full(self.quantityStructures, 1) #Plot All

        #Plot Curves
        for i in range(self.quantityStructures):
            if(mask[i] == 1):
                if(frequencyRange == None):
                    x = self.structures[i].frequencies
                    y = self.structures[i].s11
                else:
                    start = self.structures[i].GetIndex(frequencyRange[0])
                    end = self.structures[i].GetIndex(frequencyRange[1])
                    x = self.structures[i].frequencies[start:end+1]
                    y = self.structures[i].s11[start:end+1]
                
                legends.append(self.structures[i].name)
                plt.plot(x,y, linewidth = self.lineWidth)
        
        #Plot Marks
        for mark in marks:
            for structure in self.structures:
                index = structure.GetIndex(mark)
                x = structure.frequencies[index]
                y = structure.s11[index]
                
                legends.append("%.01f GHz - %s" %(mark, structure.name))
                plt.plot(x,y,marker='o')

        #Last Configuratiobns
        plt.legend(legends)
        plt.title("Return Loss")
        plt.xlabel("Frequency [GHz]")
        plt.ylabel("Return Loss [dB]")
        plt.grid(True)

        #Save with unique name
        name = "graphic"
        while(os.path.exists(self.dir + "/" + name + ".png")):
            name += "1"
        
        plt.savefig(self.dir + "/" + name + ".png")

