# Return Loss Analyzer
Package to generate reports and graphics from the results exported by the software CST Microwave Studio

Features:
* Return Loss graphics;
* Tunning bands;
* S11 at specific frequencies.
